# excel-dupes-finder
Find duplicates accross multiple Excel files or sheets

## Requirements
- python 3
- xlrd
- unidecode (used here to work with accents)
- argparse

## Usage
```bash
python3 src/main.py -f FILE [FILE ...]
```

All files must be Excel workbooks.

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂